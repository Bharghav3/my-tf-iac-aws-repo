package test

deny[msg] {
    changeset := input.resource_changes[_]
    
    required_tags := {"Name", "Owner", "Env"}
    provided_tags := {tag | changeset.change.after.tags[tag]}
    missing_tags := required_tags - provided_tags
    
    count(missing_tags) > 0
    
    msg := sprintf("Failed : Resource %v missing tags: %v", [
        changeset.address,
        concat(", ", missing_tags)
    ])
}

deny[msg] {
    changeset := input.resource_changes[_]

    changeset.type == "aws_instance"
    changeset.change.after.instance_type != "t2.micro"
    
    msg := sprintf("Failed : Instance %v is not a T2.Micro", [
        changeset.name
    ])
}

deny[msg] {
    changeset := input.resource_changes[_]

    changeset.type == "aws_security_group"
    changeset.change.after.ingress.cidr_blocks = ["0.0.0.0/0"]
    
    msg := sprintf("Security Group %v consists 0.0.0.0", [
        changeset.name
    ])
}